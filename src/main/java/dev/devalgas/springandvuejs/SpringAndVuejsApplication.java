package dev.devalgas.springandvuejs;

import dev.devalgas.springandvuejs.domain.Person;
import dev.devalgas.springandvuejs.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAndVuejsApplication implements CommandLineRunner {

    private final PersonRepository personRepository;

    public SpringAndVuejsApplication(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    public static void main(String[] args) {
        SpringApplication.run(SpringAndVuejsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        personRepository.save(Person.builder()
                .firstName("Ramesh")
                .lastName("Fadatare")
                .email("ramesh@gmail.com")
                .build());

        personRepository.save(Person.builder()
                .firstName("Tony")
                .lastName("Stark")
                .email("tony@gmail.com")
                .build());

        personRepository.save(Person.builder()
                .firstName("John")
                .lastName("Cena")
                .email("cena@gmail.com")
                .build());
    }
}
