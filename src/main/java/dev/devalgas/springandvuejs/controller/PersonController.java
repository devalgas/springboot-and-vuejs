package dev.devalgas.springandvuejs.controller;

import dev.devalgas.springandvuejs.domain.Person;
import dev.devalgas.springandvuejs.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author devalgas kamga on 05/07/2022. 16:06
 */
@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:3000/")
@Slf4j
public class PersonController {

    private final PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/persons")
    public ResponseEntity<List<Person>> findAll(){
        log.info("xjs {} : ", personRepository.findAll().toString());
        return ResponseEntity.ok(personRepository.findAll());
    }
}
