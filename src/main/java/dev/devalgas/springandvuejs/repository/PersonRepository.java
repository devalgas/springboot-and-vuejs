package dev.devalgas.springandvuejs.repository;

import dev.devalgas.springandvuejs.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author devalgas kamga on 05/07/2022. 08:26
 */
public interface PersonRepository extends JpaRepository<Person, Long> {
}
